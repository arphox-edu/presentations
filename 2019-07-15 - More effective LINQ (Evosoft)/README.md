# Evosoft-MoreEffectiveLinq
Előadásom 2019.07.15-én az evosoftnál (utolsó előtti munkanapom) LINQ és performance témában.

# Linkek
- [rövid link erre a repora](https://bit.do/linqeloadas)
- [Reference Source (.NET Framework latest (4.8))](https://referencesource.microsoft.com/)
  - [Enumerable.cs](https://referencesource.microsoft.com/#System.Core/System/Linq/Enumerable.cs) - LINQ (to Objects) implementáció
- [Optimising LINQ - Performance is a Feature!](https://mattwarren.org/2016/09/29/Optimising-LINQ/)
  - benne lévő diasorozat: 37-44 (Roslyn Performance Lessons)
  - RoslynLinqRewrite and LinqOptimizer
- [More Effective LINQ (by Mark Heath)](https://app.pluralsight.com/library/courses/linq-more-effective/table-of-contents) - A PluralSight előadás, amit részben felhasználtam
- [Roslyn coding conventions](https://github.com/dotnet/roslyn/wiki/Contributing-Code#coding-conventions)

## Egyebek
- [PLINQ](https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/parallel-linq-plinq)
- [LINQPad](https://www.linqpad.net/) - hasznos tool, érdemes kipróbálni (van céges licensz is)