﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace MoreEffectiveLinq
{
    public static class PerformanceMeasurementHelpers
    {
        public static TimeSpan Measure(Action a)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            a();
            return stopwatch.Elapsed;
        }

        public static List<TimeSpan> MeasureShuffled(int count, params Action[] actions)
        {
            List<TimeSpan>[] times = new List<TimeSpan>[actions.Length];
            for (int i = 0; i < actions.Length; i++)
                times[i] = new List<TimeSpan>(count);

            Random r = new Random();

            // Iterate actions randomly
            for (int c = 0; c < count; c++)
            {
                foreach (int i in Enumerable.Range(0, actions.Length).OrderBy(x => r.Next()))
                {
                    times[i].Add(Measure(actions[i]));
                }
            }

            return times
                .Select(t => t.Average(ts => ts.Ticks))
                .Select(ticks => new TimeSpan((long)ticks))
                .ToList();
        }
    }
}