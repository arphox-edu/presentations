﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MoreEffectiveLinq
{
    class E09_KetListaOsszehasonlitasa
    {
        // Tegyük fel, hogy van 2 tömbünk, és össze szeretnénk őket hasonlítani,
        // hogy rendre azonosak-e az elemeik...

        bool Arrays_SequenceEqual(int[] a, int[] b)
        {
            if (a.Length != b.Length)
                return false;

            for (int i = 0; i < a.Length; i++)
                if (a[i] != b[i])
                    return false;

            return true;
        }

        // Ugyanezt írjuk át egyszerűen IEnumerable<int> -re! (és nem ICollection-ök...)
        bool Enumerables_SequenceEqual(IEnumerable<int> a, IEnumerable<int> b)
        {
            if (a.Count() != b.Count())
                return false;

            for (int i = 0; i < a.Count(); i++)
                if (a.ElementAt(i) != b.ElementAt(i))
                    return false;

            return true;
        }
        
        // Vegyük ezt az egyszerű manuális implementációt
        private class ManualEnumerable : IEnumerable<int>, IEnumerator<int>
        {
            public static int MOVENEXT_CALL_COUNTER { get; private set; } = 0;
            public IEnumerator<int> GetEnumerator() => new ManualEnumerable();
            public int Current { get; private set; } = 0;
            public bool MoveNext()
            {
                Current++;
                MOVENEXT_CALL_COUNTER++;
                return Current <= 1000;
            }
            public void Dispose() { }
            public void Reset() => Current = 0;
            object IEnumerator.Current => Current;
            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        public static void Test1()
        {
            var a = new ManualEnumerable();
            var b = new ManualEnumerable();

            new E09_KetListaOsszehasonlitasa().Enumerables_SequenceEqual(a, b);

            Console.WriteLine(ManualEnumerable.MOVENEXT_CALL_COUNTER); // 2_005_003 hívás!
        }

        public static void AhogyKellene_SequenceEqual()
        {
            var a = new ManualEnumerable();
            var b = new ManualEnumerable();

            a.SequenceEqual(b);

            Console.WriteLine(ManualEnumerable.MOVENEXT_CALL_COUNTER); // 2002 hívás
        }

        // A .NET-es implementáció (picit egyszerűsítve, nem generikusan)
        bool SequenceEqual_ImplementationDotNet(IEnumerable<int> a, IEnumerable<int> b)
        {
            using (IEnumerator<int> e1 = a.GetEnumerator())
            using (IEnumerator<int> e2 = b.GetEnumerator())
            {
                while (e1.MoveNext())
                    if (!e2.MoveNext() || e1.Current != e2.Current)
                        return false;

                if (e2.MoveNext())
                    return false;
            }

            return true;
        }
    }
}