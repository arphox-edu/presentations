﻿using System.Collections.Generic;
using System.Linq;

namespace MoreEffectiveLinq
{
    class E15_LinqFun_Last
    {
        // Hogyan (ne) vegyük ki az utolsó elemet egy gyűjteményből??

        static void Code<T>(IEnumerable<T> sequence)
        {
            // 1.
            var last = sequence.Reverse().First(); // A Reverse el"cache"-eli az egész gyűjteményt

            // 2.
            last = sequence.ToArray()[sequence.Count() - 1]; // wat?

            // 3.
            last = sequence.Last(); // -> na, így kellene.

            // -> HASZNOS, ha ismerjük az összes Linq-s metódust! Nézzünk utána!
        }
    }
}