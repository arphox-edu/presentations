﻿using System.Collections.Generic;

namespace MoreEffectiveLinq
{
    class E02_YieldReturn
    {
        IEnumerable<int> GetNumbers()
        {
            yield return 1;
            yield return 2;
            yield return 3;
            yield return 4;
            yield return 5;
        }

        /*
           Ennek a compiler-implementációjában lesz:
             - Thread-függő kezelés
             - egy state machine
             - stb.

            => NEM EGYSZERŰ, de erre a témára még visszatérünk...
        */
    }
}