﻿using System.Linq;

namespace MoreEffectiveLinq
{
    class E13_LinqCsapda_1
    {
        // LINQ csapda 1: belső lekérdezések többször kiértékelve

        // Feladat: Melyik könyvnek (az objektum kell) van a legtöbb oldala?

        private class Book
        {
            public string Title { get; set; }
            public int Pages { get; set; }
        }

        private static readonly Book[] books = new Book[]
        {
            new Book(){ Title = "A", Pages = 100 },
            new Book(){ Title = "B", Pages = 200 },
            new Book(){ Title = "C", Pages = 300 }
        };


        void Code()
        {
            // Naív (és lassú!):
            Book longest1 = books.First(b => b.Pages == books.Max(x => x.Pages));

            // Gyorsabb:
            int maxValue = books.Max(x => x.Pages);
            Book longest2 = books.First(b => b.Pages == maxValue);
        }
    }
}