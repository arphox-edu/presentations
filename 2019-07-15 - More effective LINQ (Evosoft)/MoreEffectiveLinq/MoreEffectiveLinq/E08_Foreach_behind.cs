﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MoreEffectiveLinq
{
    static class E08_Foreach_behind
    {
        static void Foreach<T>(IEnumerable<T> source)
        {
            foreach (T item in source)
            {
                // do something
            }
        }





        static void ManualForeach<T>(IEnumerable<T> source)
        {
            using (IEnumerator<T> enumerator = source.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    T item = enumerator.Current;
                    // do something
                }
            }

            // Ebben az esetben legalábbis...
            // (De: IEnumerable, duck typing, stb. ...)
        }





        static int MyCount2<T>(IEnumerable<T> source)
        {
            int count = 0;

            using (IEnumerator<T> e = source.GetEnumerator())
                while (e.MoveNext())
                    count++; // ez lehetne checked...

            return count;
        }





        // És most nézzük, hogy hogy van ez implementálva a .NET-ben:
        public static int DotNetBuiltIn_Count<TSource>(this IEnumerable<TSource> source)
        {
            if (source == null) throw new ArgumentNullException("source");
            ICollection<TSource> collectionoft = source as ICollection<TSource>;
            if (collectionoft != null) return collectionoft.Count;
            ICollection collection = source as ICollection;
            if (collection != null) return collection.Count;
            int count = 0;
            using (IEnumerator<TSource> e = source.GetEnumerator())
            {
                checked
                {
                    while (e.MoveNext()) count++;
                }
            }
            return count;
        }
    }
}