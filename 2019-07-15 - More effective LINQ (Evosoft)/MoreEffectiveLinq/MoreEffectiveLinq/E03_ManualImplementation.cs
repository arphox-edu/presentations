﻿using System.Collections;
using System.Collections.Generic;

namespace MoreEffectiveLinq
{
    class E03_ManualImplementation
    {
        public IEnumerable<int> GetNumbers()
        {
            return new GetNumbersEnumerable();
        }

        private class GetNumbersEnumerable : IEnumerable<int>
        {
            public IEnumerator<int> GetEnumerator() => new GetNumbersEnumerator();
            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        private class GetNumbersEnumerator : IEnumerator<int>
        {
            private int counter = 0;

            public int Current => counter;
            public bool MoveNext()
            {
                counter++;
                return counter <= 5;
            }

            public void Dispose() { }
            public void Reset() => throw new System.NotImplementedException();
            object IEnumerator.Current => Current;
        }

        #region [ 2in1 implementation ]
        private class GetNumbers2in1 : IEnumerable<int>, IEnumerator<int>
        {
            // IEnumerable <int>
            public IEnumerator<int> GetEnumerator() => new GetNumbers2in1();
            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            // IEnumerator <int>
            private int counter = 0;
            public int Current => counter;
            public bool MoveNext()
            {
                counter++;
                return counter <= 5;
            }
            public void Dispose() { }
            public void Reset() => throw new System.NotImplementedException();
            object IEnumerator.Current => Current;
        }
        #endregion
    }
}