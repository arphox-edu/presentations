﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoreEffectiveLinq
{
    class E07_LINQ_Bevezeto
    {
        /* 
         LINQ - [L]anguage [IN]tegrated [Q]uery

        - .NET 3.5 (2007) óta
        - extension method-ökre épül
        - IEnumerable<T> / IQuaryable<T> -t terjeszti ki (mi a különbség?)
        - query és method syntax ↓
        */

        void Query_VS_Method()
        {
            int[] scores = { 18, 52, 77, 90 };

            // Query syntax
            IEnumerable<int> querySyntax =
                from score in scores
                where score > 80
                select score;

            // Method syntax
            IEnumerable<int> methodSyntax = scores.Where(s => s > 80);

            // A Query syntax method syntax-ra fordul!
        }

        /* - KÉSLELTETETT (deferred) végrehajtás!
            -> yield...
        */

        void LINQ_pipeline()
        {
            string[] arr = { "Anna", "Botond", "fű", "Emil", "keksz", "Feri", "Laci", "Zoli" };

            int x = arr
                .Where(s => !string.IsNullOrWhiteSpace(s)) // nem null és ""
                .Where(s => char.IsUpper(s[0])) // nagybetűvel kezdődik (=név)
                .Select(s => s.Last()) // utolsó karakter
                .Distinct() // csak az egyediek
                .Count(); // hány darab?

            Console.WriteLine(x); // Hány darab különböző utolsó betű van a nevekben? 
        }

        // Példa: Count() saját kézzel
        int MyCount<T>(IEnumerable<T> source)
        {
            // null check
            // ICollection<T> / ICollection amire cast-olgathatnánk és return .Count

            int count = 0;
            foreach (T item in source) // de amúgy mi az a foreach ???
                count++;

            return count;
        }
    }
}