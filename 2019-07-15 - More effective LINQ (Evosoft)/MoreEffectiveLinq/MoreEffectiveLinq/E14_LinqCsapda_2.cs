﻿using System.Collections.Generic;
using System.Linq;

namespace MoreEffectiveLinq
{
    class E14_LinqCsapda_2
    {
        // LINQ csapda 2: ".Count() > 0" vs "Any()"

        // Feladat: Van-e 20 oldalas könyv?

        private class Book
        {
            public string Title { get; set; }
            public int Pages { get; set; }
        }

        private static readonly IEnumerable<Book> books = new Book[]
        {
            new Book(){ Title = "A", Pages = 100 },
            new Book(){ Title = "B", Pages = 200 },
            new Book(){ Title = "C", Pages = 300 }
        };


        void Code()
        {
            // Lassabb:
            bool result1 = books.Where(b => b.Pages == 20).Count() > 0;
            bool result2 = books.Count(b => b.Pages == 20) > 0; // ugyanaz

            // Gyorsabb:
            bool result3 = books.Any(b => b.Pages == 20);
        }
    }
}