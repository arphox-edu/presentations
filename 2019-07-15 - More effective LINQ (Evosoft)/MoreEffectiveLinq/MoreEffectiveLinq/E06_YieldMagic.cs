﻿using System;
using System.Collections.Generic;

namespace MoreEffectiveLinq
{
    class E06_YieldMagic
    {
        public int SomeState { get; set; } // ez kívülről is beállítható

        public IEnumerable<int> GetNumbers()
        {
            yield return 1;

            if (SomeState > 0)
            {
                yield return 2;
            }
            else
            {
                yield return 3;
            }
        }


        // --------------------------------------------------------
        // Példa:
        void Example()
        {
            // "1 3"
            var obj1 = new E06_YieldMagic();
            foreach (var x in obj1.GetNumbers())
            {
                Console.Write(x + " ");
            }

            // "1 2"
            var obj2 = new E06_YieldMagic();
            foreach (var x in obj2.GetNumbers())
            {
                Console.Write(x + " ");
                obj2.SomeState = 7;
            }
        }
    }
}