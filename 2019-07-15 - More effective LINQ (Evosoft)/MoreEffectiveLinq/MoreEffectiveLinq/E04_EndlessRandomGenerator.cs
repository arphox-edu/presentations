﻿using System;
using System.Collections.Generic;

namespace MoreEffectiveLinq
{
    class E04_EndlessRandomGenerator
    {
        public IEnumerable<int> GenerateRandomNumbers()
        {
            Random random = new Random();
            while (true)
                yield return random.Next();
        }
    }
}