﻿/*

BenchmarkDotNet=v0.11.5, OS=Windows 10.0.17134.829 (1803/April2018Update/Redstone4)
Intel Core i7-6700 CPU 3.40GHz (Skylake), 1 CPU, 8 logical and 4 physical cores
Frequency=3328127 Hz, Resolution=300.4693 ns, Timer=TSC
  [Host]     : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3416.0
  DefaultJob : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3416.0


 Method |      Mean |    Error |   StdDev | Ratio | RatioSD | Gen 0 | Gen 1 | Gen 2 | Allocated |
------- |----------:|---------:|---------:|------:|--------:|------:|------:|------:|----------:|
 Manual |  74.20 ms | 1.422 ms | 1.461 ms |  1.00 |    0.00 |     - |     - |     - |         - |
   Linq | 116.65 ms | 2.348 ms | 2.610 ms |  1.57 |    0.05 |     - |     - |     - |         - |


*/

using System;
using System.Linq;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
    [MarkdownExporter]
    [MemoryDiagnoser]
    public class E11_Benchmark
    {
        // Feladat: "R" betűvel kezdődő nevű emberek átlagéletkora

        const int COLLETION_SIZE = 10_000_000;

        private static readonly string[] nameBases = { "John", "Ram", "Steve", "Bill", "Ron", "Dickon" };
        private Person[] people;

        [GlobalSetup]
        public void Setup()
        {
            Random r = new Random(2019);

            people = new Person[COLLETION_SIZE];
            for (int i = 0; i < COLLETION_SIZE; i++)
            {
                people[i] = new Person()
                {
                    Name = nameBases[r.Next(0, nameBases.Length)] + i.ToString(),
                    Age = r.Next(10, 70)
                };
            };
        }

        [Benchmark(Baseline = true)]
        public double Manual() => Impl_Manual();

        [Benchmark()]
        public double Linq() => Impl_Linq();

        public class Person
        {
            public string Name { get; set; }
            public int Age { get; set; }
        }

        private double Impl_Linq()
        {
            return people
                .Where(p => p.Name[0] == 'R')
                .Average(p => p.Age);
        }

        private double Impl_Manual()
        {
            double sum = 0;

            foreach (Person p in people)
                if (p.Name[0] == 'R')
                    sum += p.Age;

            return sum;
        }
    }
}
