﻿using System;
using System.Diagnostics;
using BenchmarkDotNet.Running;

namespace Benchmarks
{
    class Program
    {
        static void Main(string[] args)
        {
#if DEBUG
            throw new Exception("Run this in RELEASE mode!");
#endif
            if (Debugger.IsAttached)
                throw new Exception("Do not attach a debugger!");

            //BenchmarkRunner.Run<E05_Benchmark>();
            //BenchmarkRunner.Run<E10_Benchmark>();
            //BenchmarkRunner.Run<E11_Benchmark>();
            //BenchmarkRunner.Run<E12_Benchmark>();
            BenchmarkRunner.Run<E16_Benchmark>();
        }
    }
}