﻿/*

BenchmarkDotNet=v0.11.5, OS=Windows 10.0.17134.829 (1803/April2018Update/Redstone4)
Intel Core i7-6700 CPU 3.40GHz (Skylake), 1 CPU, 8 logical and 4 physical cores
Frequency=3328127 Hz, Resolution=300.4693 ns, Timer=TSC
  [Host]     : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3416.0
  DefaultJob : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3416.0


                   Method |     Mean |    Error |   StdDev | Ratio | RatioSD |   Allocated |
------------------------- |---------:|---------:|---------:|------:|--------:|------------:|
             Manual_Array | 378.0 ms | 7.874 ms | 6.981 ms |  1.00 |    0.00 | 200008216 B |
 Manual_List_CapacityCtor | 506.4 ms | 8.426 ms | 7.881 ms |  1.34 |    0.04 | 200008216 B |
  Manual_List_DefaultCtor | 637.3 ms | 9.368 ms | 8.305 ms |  1.69 |    0.04 | 536887776 B |
                    Yield | 253.6 ms | 4.927 ms | 4.839 ms |  0.67 |    0.01 |           - |



    Konklúzió -> okosan kell használni!

    Néhány ötlet ↓

    YIELD:

        Miért igen?
    + egyszerűen érthető & olvasható kód
    + végtelen / számolt listákhoz
    + lazy -> annyit ad, amennyit kérnek tőle (emiatt gyorsabb lehet)
    + kevesebb memóriahasználat (a kézi IEnumerable<T> / IEnumerator<T> implementáció még jobb lehet)

        Miért ne?
    - a debugolását meg kell tanulni és érteni
    - ha kívül ToList/ToArray hívás történik rá, máris lassabb egy picit
    - rejtett kód generálódik (picit nagyobb DLL méret, néha ez számíthat)
    - veszélyes is lehet (lásd következő példa)

        
    Akkor NE használjuk, ha:
    - minden eredmény legalább egyszer BIZTOSAN kelleni fog ÉS
    - a hívó oldalon nem lesz gyűjteménymásolgatás (ToArray / ToList, Array.Copy, stb.)

    -> de ezekben az esetben van egyáltalán értelme IEnumerable<T>-t használni ? -> gondoljuk át!
*/

using System.Collections.Generic;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
    [MarkdownExporter]
    [MemoryDiagnoser]
    public class E05_Benchmark
    {
        const int COLLETION_SIZE = 50_000_000;

        [Benchmark(Baseline = true)]
        public int Manual_Array() => Count(Impl_Manual_Array());
        [Benchmark]
        public int Manual_List_CapacityCtor() => Count(Impl_Manual_List_CapacityCtor());
        [Benchmark]
        public int Manual_List_DefaultCtor() => Count(Impl_Manual_List_DefaultCtor());
        [Benchmark]
        public int Yield() => Count(Impl_Yield());


        public IEnumerable<int> Impl_Manual_Array()
        {
            int[] arr = new int[COLLETION_SIZE];

            for (int i = 0; i < COLLETION_SIZE; i++)
                arr[i] = i;

            return arr;
        }

        public IEnumerable<int> Impl_Manual_List_CapacityCtor()
        {
            List<int> list = new List<int>(COLLETION_SIZE);

            for (int i = 0; i < COLLETION_SIZE; i++)
                list.Add(i);

            return list;
        }

        public IEnumerable<int> Impl_Manual_List_DefaultCtor()
        {
            List<int> list = new List<int>();

            for (int i = 0; i < COLLETION_SIZE; i++)
                list.Add(i);

            return list;
        }

        public IEnumerable<int> Impl_Yield()
        {
            for (int i = 0; i < COLLETION_SIZE; i++)
                yield return i;
        }

        public int Count(IEnumerable<int> numbers)
        {
            int count = 0;

            foreach (var item in numbers)
                count++;

            return count;
        }
    }
}