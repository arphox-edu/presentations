﻿/*

BenchmarkDotNet=v0.11.5, OS=Windows 10.0.17134.829 (1803/April2018Update/Redstone4)
Intel Core i7-6700 CPU 3.40GHz (Skylake), 1 CPU, 8 logical and 4 physical cores
Frequency=3328127 Hz, Resolution=300.4693 ns, Timer=TSC
  [Host]     : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3416.0
  DefaultJob : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3416.0


                       Method |     Mean |    Error |   StdDev | Ratio | RatioSD |     Gen 0 | Gen 1 | Gen 2 | Allocated |
----------------------------- |---------:|---------:|---------:|------:|--------:|----------:|------:|------:|----------:|
               Manual_HashSet | 155.4 ms | 3.833 ms | 4.414 ms |  1.00 |    0.00 |         - |     - |     - |  24.78 MB |
 Manual_HashSet_CapacityCheat | 154.9 ms | 3.337 ms | 4.678 ms |  1.00 |    0.05 |         - |     - |     - |   8.91 MB |
                   Manual_Set | 153.8 ms | 3.416 ms | 4.066 ms |  0.99 |    0.03 |         - |     - |     - |     20 MB |
                         Linq | 206.4 ms | 3.275 ms | 2.735 ms |  1.33 |    0.03 | 3000.0000 |     - |     - |   32.7 MB |

*/

using System;
using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
    [MarkdownExporter]
    [MemoryDiagnoser]
    public class E12_Benchmark
    {
        // Feladat: "R" betűvel kezdődő nevű emberek egyedi barátainak darabszáma összesen

        const int COLLETION_SIZE = 1_000_000;

        private static readonly string[] nameBases = { "John", "Ram", "Steve", "Bill", "Ron", "Dickon" };
        private Person[] people;

        [GlobalSetup]
        public void Setup()
        {
            Random r = new Random(2019); // fix randomness

            people = new Person[COLLETION_SIZE];
            for (int i = 0; i < COLLETION_SIZE; i++)
            {
                people[i] = new Person()
                {
                    Name = nameBases[r.Next(0, nameBases.Length)] + i.ToString(),
                    Age = r.Next(10, 70),
                };

                int friendCount = r.Next(0, Math.Min(i, r.Next(1, 10)));
                for (int j = 0; j < friendCount; j++)
                    people[i].Friends.Add(people[r.Next(0, i)]);
            };
        }

        [Benchmark(Baseline = true)]
        public int Manual_HashSet() => Impl_Manual_HashSet();

        [Benchmark]
        public int Manual_HashSet_CapacityCheat() => Impl_Manual_HashSet_CapacityCheat();

        [Benchmark]
        public int Manual_Set() => Impl_Manual_Set();

        [Benchmark]
        public int Linq() => Impl_Linq();

        private int Impl_Linq()
        {
            return people
                .Where(p => p.Name[0] == 'R')
                .SelectMany(p => p.Friends)
                .Distinct()
                .Count();
        }

        private int Impl_Manual_HashSet()
        {
            HashSet<Person> allFriends = new HashSet<Person>();

            foreach (Person p in people)
                if (p.Name[0] == 'R')
                    foreach (var friend in p.Friends)
                        allFriends.Add(friend);

            return allFriends.Count;
        }

        private int Impl_Manual_HashSet_CapacityCheat()
        {
            HashSet<Person> allFriends = new HashSet<Person>(400_000);

            foreach (Person p in people)
                if (p.Name[0] == 'R')
                    foreach (var friend in p.Friends)
                        allFriends.Add(friend);

            return allFriends.Count;
        }

        private int Impl_Manual_Set()
        {
            Set<Person> allFriends = new Set<Person>();

            foreach (Person p in people)
                if (p.Name[0] == 'R')
                    foreach (var friend in p.Friends)
                        allFriends.Add(friend);

            return allFriends.Count;
        }

        public class Person : IEquatable<Person>
        {
            public string Name { get; set; }
            public int Age { get; set; }
            public List<Person> Friends { get; } = new List<Person>();

            public bool Equals(Person other) => other.Name == Name && other.Age == Age;
            public override int GetHashCode() => Name.GetHashCode() + Age.GetHashCode();
            public override bool Equals(object obj) => Equals((Person)obj);
        }

        private class Set<TElement> // this is used for Distinct()
        { // stolen from .NET impl: https://referencesource.microsoft.com/#System.Core/System/Linq/Enumerable.cs,2380
            int[] buckets;
            Slot[] slots;
            public int Count { get; private set; } // made it public
            int freeList;
            IEqualityComparer<TElement> comparer;
            public Set() : this(null) { }
            public Set(IEqualityComparer<TElement> comparer)
            {
                if (comparer == null) comparer = EqualityComparer<TElement>.Default;
                this.comparer = comparer;
                buckets = new int[7];
                slots = new Slot[7];
                freeList = -1;
            }
            public bool Add(TElement value) => !Find(value, true);
            bool Find(TElement value, bool add)
            {
                int hashCode = InternalGetHashCode(value);
                for (int i = buckets[hashCode % buckets.Length] - 1; i >= 0; i = slots[i].next)
                {
                    if (slots[i].hashCode == hashCode && comparer.Equals(slots[i].value, value)) return true;
                }
                if (add)
                {
                    int index;
                    if (freeList >= 0)
                    {
                        index = freeList;
                        freeList = slots[index].next;
                    }
                    else
                    {
                        if (Count == slots.Length) Resize();
                        index = Count;
                        Count++;
                    }
                    int bucket = hashCode % buckets.Length;
                    slots[index].hashCode = hashCode;
                    slots[index].value = value;
                    slots[index].next = buckets[bucket] - 1;
                    buckets[bucket] = index + 1;
                }
                return false;
            }

            void Resize()
            {
                int newSize = checked(Count * 2 + 1);
                int[] newBuckets = new int[newSize];
                Slot[] newSlots = new Slot[newSize];
                Array.Copy(slots, 0, newSlots, 0, Count);
                for (int i = 0; i < Count; i++)
                {
                    int bucket = newSlots[i].hashCode % newSize;
                    newSlots[i].next = newBuckets[bucket] - 1;
                    newBuckets[bucket] = i + 1;
                }
                buckets = newBuckets;
                slots = newSlots;
            }
            internal int InternalGetHashCode(TElement value) => (value == null) ? 0 : comparer.GetHashCode(value) & 0x7FFFFFFF;
            internal struct Slot
            {
                internal int hashCode;
                internal TElement value;
                internal int next;
            }
        }
    }
}
