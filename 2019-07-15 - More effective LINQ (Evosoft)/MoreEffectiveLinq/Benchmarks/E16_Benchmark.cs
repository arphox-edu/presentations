﻿/*

BenchmarkDotNet=v0.11.5, OS=Windows 10.0.17134.829 (1803/April2018Update/Redstone4)
Intel Core i7-6700 CPU 3.40GHz (Skylake), 1 CPU, 8 logical and 4 physical cores
Frequency=3328127 Hz, Resolution=300.4693 ns, Timer=TSC
  [Host]     : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3416.0
  DefaultJob : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3416.0


          Method |     Mean |    Error |   StdDev | Ratio | RatioSD |
---------------- |---------:|---------:|---------:|------:|--------:|
    Array_Simple | 146.9 ms | 2.102 ms | 1.864 ms |  1.00 |    0.00 |
 Array_Extracted | 148.2 ms | 2.363 ms | 2.210 ms |  1.01 |    0.03 |
     List_Simple | 149.8 ms | 2.939 ms | 4.023 ms |  1.02 |    0.04 |
  List_Extracted | 148.4 ms | 2.538 ms | 2.374 ms |  1.01 |    0.01 |

*/

using System.Collections.Generic;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
    [MarkdownExporter]
    public class E16_Benchmark
    {
        private int[] array;
        private List<int> list;

        [GlobalSetup]
        public void Setup()
        {
            const int COLLETION_SIZE = 536_000_000;

            array = new int[COLLETION_SIZE];
            list = new List<int>(COLLETION_SIZE);

            for (int i = 0; i < COLLETION_SIZE; i++)
                list.Add(0);
        }

        [Benchmark(Baseline = true)]
        public int Array_Simple() => Impl_Array_Simple();

        [Benchmark]
        public int Array_Extracted() => Impl_Array_Extracted();

        [Benchmark]
        public int List_Simple() => Impl_List_Simple();

        [Benchmark]
        public int List_Extracted() => Impl_List_Extracted();



        private int Impl_Array_Simple()
        {
            int count = 0;
            for (int i = 0; i < array.Length; i++)
                count++;

            return count;
        }

        private int Impl_Array_Extracted()
        {
            int count = 0;
            int max = array.Length;
            for (int i = 0; i < max; i++)
                count++;

            return count;
        }

        private int Impl_List_Simple()
        {
            int count = 0;
            for (int i = 0; i < list.Count; i++)
                count++;

            return count;
        }

        private int Impl_List_Extracted()
        {
            int count = 0;
            int max = list.Count;
            for (int i = 0; i < max; i++)
                count++;

            return count;
        }
    }
}
