﻿/*

BenchmarkDotNet=v0.11.5, OS=Windows 10.0.17134.829 (1803/April2018Update/Redstone4)
Intel Core i7-6700 CPU 3.40GHz (Skylake), 1 CPU, 8 logical and 4 physical cores
Frequency=3328127 Hz, Resolution=300.4693 ns, Timer=TSC
  [Host]     : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3416.0
  DefaultJob : .NET Framework 4.7.2 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.3416.0


     Method |      Mean |     Error |    StdDev | Ratio | RatioSD |
----------- |----------:|----------:|----------:|------:|--------:|
      Array |  88.37 ms |  1.456 ms |  1.362 ms |  1.00 |    0.00 |
  ArrayLinq | 562.78 ms |  6.649 ms |  5.895 ms |  6.36 |    0.13 |
 ArrayIList | 873.82 ms | 13.876 ms | 12.980 ms |  9.89 |    0.23 |

       List | 135.56 ms |  2.691 ms |  3.684 ms |  1.54 |    0.06 |
   ListLinq | 730.55 ms | 11.677 ms | 10.923 ms |  8.27 |    0.16 |
  ListIList | 811.62 ms | 12.045 ms | 11.267 ms |  9.19 |    0.20 |

  
Tehát:
    -> az interfészek rontanak a helyzeten...
        -> minél alacsonyabb szintű a kód, annál gyorsabb...
    (de ne felejtsük el, hogy ez a LINQ nem használ yield-et!)

*/

using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
    [MarkdownExporter]
    [MemoryDiagnoser]
    public class E10_Benchmark
    {
        const int COLLETION_SIZE = 100_000_000;

        private int[] array;
        private List<int> list;

        [GlobalSetup]
        public void GlobalSetup()
        {
            array = new int[COLLETION_SIZE];
            list = new List<int>(COLLETION_SIZE);
            for (int i = 0; i < COLLETION_SIZE; i++)
                list.Add(i);
        }

        [Benchmark(Baseline = true)]
        public int Array() => MaxIndex_Array(array);
        [Benchmark]
        public int ArrayLinq() => array.Max();
        [Benchmark]
        public int ArrayIList() => MaxIndex_IList(array);


        [Benchmark]
        public int List() => MaxIndex_List(list);
        [Benchmark]
        public int ListLinq() => list.Max();
        [Benchmark]
        public int ListIList() => MaxIndex_IList(list);


        public int MaxIndex_Array(int[] array)
        {
            int maxi = 0;
            for (int i = 1; i < array.Length; i++)
                if (array[i] > array[maxi])
                    maxi = i;
            return maxi;
        }

        public int MaxIndex_List(List<int> list)
        {
            int maxi = 0;
            for (int i = 1; i < list.Count; i++)
                if (list[i] > list[maxi])
                    maxi = i;
            return maxi;
        }

        public int MaxIndex_IList(IList<int> list)
        {
            int maxi = 0;
            for (int i = 1; i < list.Count; i++)
                if (list[i] > list[maxi])
                    maxi = i;
            return maxi;
        }
    }
}